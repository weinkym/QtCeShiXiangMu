﻿#ifndef LANGHELP_H
#define LANGHELP_H

#include <QDialog>

namespace Ui {
class LangHelp;
}

class LangHelp : public QDialog
{
    Q_OBJECT

public:
    explicit LangHelp(QWidget *parent = nullptr);
    ~LangHelp();

private slots:
    void on_pushButton_clicked();

private:
    Ui::LangHelp *ui;
};

#endif // LANGHELP_H
