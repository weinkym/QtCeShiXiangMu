﻿#ifndef PARSETHREAD_H
#define PARSETHREAD_H

#include <QObject>
#include <QThread>

class ParseThread : public QObject
{
    Q_OBJECT
public:
    explicit ParseThread(QObject *parent = nullptr);

signals:

public slots:
    void doWork();
    void Stop();
};

#endif // PARSETHREAD_H
