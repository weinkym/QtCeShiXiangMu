﻿#include "widget.h"
#include "ui_widget.h"
#include <QDebug>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
    , btnSwitchTab(new QPushButton(QString::fromLocal8Bit("切换Tab")))
    , btnSwitchDir(new QPushButton(QString::fromLocal8Bit("切换方向")))
    , btnSwitchStyle(new QPushButton(QString::fromLocal8Bit("切换样式")))
    , hLayOP(new QHBoxLayout)
    , Tab1(new QWidget)
    , Tab2(new QWidget)
    , Tab3(new QWidget)
    , tabWidgetMain(new QTabWidget)
    , vLayMain(new QVBoxLayout)
{
    ui->setupUi(this);
    hLayOP->addWidget(btnSwitchTab);
    hLayOP->addWidget(btnSwitchDir);
    hLayOP->addWidget(btnSwitchStyle);
    hLayOP->addStretch();
    vLayMain->addLayout(hLayOP);
    vLayMain->addWidget(tabWidgetMain);
    setLayout(vLayMain);
    // Tab
    tabWidgetMain->addTab(Tab1, QString::fromLocal8Bit("手动定位"));
    tabWidgetMain->addTab(Tab2, QString::fromLocal8Bit("自动定位"));
    tabWidgetMain->addTab(Tab3, QString::fromLocal8Bit("定位显示"));
    // connect
    connect(btnSwitchTab, &QPushButton::clicked, this, &Widget::btnSwitch_clicked);
    connect(btnSwitchDir, &QPushButton::clicked, this, &Widget::btnSwitchDir_clicked);
    // style
    InitStyle();
    tabWidgetMain->setStyleSheet(styleList.at(0));
}

Widget::~Widget()
{
    btnSwitchTab->deleteLater();
    btnSwitchDir->deleteLater();
    btnSwitchStyle->deleteLater();
    hLayOP->deleteLater();
    Tab1->deleteLater();
    Tab2->deleteLater();
    Tab3->deleteLater();
    tabWidgetMain->deleteLater();
    vLayMain->deleteLater();
    delete ui;
}

void Widget::InitStyle()
{
    QString style = "QTabWidget::tab-bar{left: 5px;}"
                    "QTabBar::tab{color:#333333;font:14px Microsoft YaHei;min-width:80px;min-height:39px;}"
                    "QTabBar::tab:selected{color: #4796f0;}"
                    "QTabBar::tab:hover{color: #00CCFF;border-bottom: 4px solid #00CCFF;}";
    styleList.push_back(style);
}

void Widget::btnSwitch_clicked()
{
    int index = tabWidgetMain->currentIndex();
    int count = tabWidgetMain->count();
    if(index + 1 == count) {
        index = 0;
        tabWidgetMain->setCurrentIndex(index);
    }
    else {
        tabWidgetMain->setCurrentIndex(++index);
    }
}


void Widget::btnSwitchDir_clicked()
{
    tabWidgetMain->setTabPosition(QTabWidget::South);
}

void Widget::btnSwitchStyle_clicked()
{

}

