#-------------------------------------------------
#
# Project created by QtCreator 2020-02-18T16:36:56
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MatlabDemo
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
#Matlab
DEFINES += __STDC_UTF_16__

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        widget.cpp

HEADERS += \
        add.h \
        derivative.h \
        widget.h

FORMS += \
        widget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

#Matlab
INCLUDEPATH += C:/MATLAB/CompilerRuntime/v713/extern/include
LIBS += -LC:/MATLAB/CompilerRuntime/v713/extern/lib/win32/microsoft -lmclmcr
LIBS += -LC:/MATLAB/CompilerRuntime/v713/extern/lib/win32/microsoft -lmclmcrrt
LIBS += -LC:/MATLAB/CompilerRuntime/v713/extern/lib/win32/microsoft -llibmx

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/./ -lderivative
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/./ -lderivative

INCLUDEPATH += $$PWD/.
DEPENDPATH += $$PWD/.

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/./ -lad
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/./ -ladd

INCLUDEPATH += $$PWD/.
DEPENDPATH += $$PWD/.
