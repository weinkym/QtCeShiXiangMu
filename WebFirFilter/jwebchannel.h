﻿#ifndef JWEBCHANNEL_H
#define JWEBCHANNEL_H

#include <QObject>
#include <QWebEnginePage>
#include <QJsonObject>
#include <QJsonDocument>

class JWebChannel : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QJsonObject jsonData MEMBER jsonData NOTIFY SendSrcData)
public:
    explicit JWebChannel(QObject *parent = nullptr);
    // 向页面发送消息
    void SendMsg(QWebEnginePage* page, const QString &msg);
signals:
    void RecvMsg(const QString &msg);
    void SendSrcData(const QJsonObject &data);
public slots:
    // 接受页面发送的信息
    void RecvJSMsg(const QString &msg);
private:
    QJsonObject jsonData;
};

#endif // JWEBCHANNEL_H
