#ifndef COMPLEX_H
#define COMPLEX_H


class Complex
{
public:
    Complex(double r = 0, double i = 0);
    Complex(const Complex &c);

    double real() const;
    double image() const;

    void setReal(double r);
    void setImage(double i);

private:
    double m_real;
    double m_image;
};

#endif // COMPLEX_H
