﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDir>
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
#include <QDebug>
#include <QDesktopServices>

#pragma execution_character_set("utf-8")

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , m_subDir(true)
{
    ui->setupUi(this);

    m_menu = new QMenu(this);
    m_menu->addAction(ui->actionOpenDir);

    ui->tableWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    ui->tableWidget->horizontalHeader()->setStretchLastSection(true);
}

MainWindow::~MainWindow()
{
    delete ui;
}

QStringList MainWindow::getFilters()
{
    QString temp = ui->lineEditFilter->text();
    QStringList filter = temp.split(",");
    for(int i = filter.size() - 1; i >= 0; i--)
    {
        if(filter.at(i).isEmpty())
        {
            filter.removeAt(i);
        }
    }

    return filter;
}

void MainWindow::filterFiles(const QString &folder)
{
    QDir dir(folder);


    if(m_subDir)
    {
        QStringList dirs = dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
        for(auto d : dirs)
        {
            filterFiles(folder + "/" + d);
        }
    }

    QStringList files = dir.entryList(m_filter);
    for(auto d : files)
    {
        m_files << folder + "/" + d;
    }
}


void MainWindow::searchString(const QString &content)
{
    ui->tableWidget->setRowCount(0);
    for(auto d : m_files)
    {
        QFile f(d);
        if(!f.open(QIODevice::ReadOnly))
        {
            qInfo() << "open file failed: " << f.errorString() << d;
            continue;
        }
        int num = 1;
        while (!f.atEnd())
        {
            QString line = f.readLine();
            if(line.contains(content))
            {
                AddTableRow(d, num, line);
            }

            ++num;
        }
        f.close();
    }
}

void MainWindow::AddTableRow(const QString &name, int line, const QString &content)
{
    int rowCount = ui->tableWidget->rowCount();
    ui->tableWidget->setRowCount(rowCount + 1);

    QTableWidgetItem *item = ui->tableWidget->item(rowCount, 0);
    if(Q_NULLPTR == item)
    {
        item = new QTableWidgetItem;
        ui->tableWidget->setItem(rowCount, 0, item);
    }
    item->setText(name);

    item = ui->tableWidget->item(rowCount, 1);
    if(Q_NULLPTR == item)
    {
        item = new QTableWidgetItem;
        ui->tableWidget->setItem(rowCount, 1, item);
    }
    item->setText(QString::number(line));

    item = ui->tableWidget->item(rowCount, 2);
    if(Q_NULLPTR == item)
    {
        item = new QTableWidgetItem;
        ui->tableWidget->setItem(rowCount, 2, item);
    }
    item->setText(content);
}


void MainWindow::on_pushButton_clicked()
{
    m_folder = QFileDialog::getExistingDirectory(this, "选择文件夹");
    ui->lineEditDir->setText(m_folder);
    if(m_folder.isEmpty())
    {
        ui->statusBar->showMessage("文件夹为空", 5000);
        return;
    }

    m_filter = getFilters();

    m_files.clear();
    filterFiles(m_folder);
}

void MainWindow::on_lineEditFilter_editingFinished()
{
    if(m_folder.isEmpty())
    {
        ui->statusBar->showMessage("文件夹为空", 5000);
        return;
    }

    m_filter = getFilters();

    m_files.clear();
    filterFiles(m_folder);
}

void MainWindow::on_lineEditDir_editingFinished()
{
    m_folder = ui->lineEditDir->text();
    if(m_folder.isEmpty())
    {
        ui->statusBar->showMessage("文件夹为空", 5000);
        return;
    }

    m_filter = getFilters();

    m_files.clear();
    filterFiles(m_folder);
}

// 查找内容
void MainWindow::on_lineEditString_editingFinished()
{
    if(m_files.isEmpty())
    {
        ui->statusBar->showMessage("没有查找到相关文件", 5000);
        return;
    }

    QString str = ui->lineEditString->text();
    if(str.isEmpty())
    {
        ui->statusBar->showMessage("请输入查找内容", 5000);
        return;
    }

    searchString(str);
}


// 查找内容
void MainWindow::on_btnSearch_clicked()
{
    if(m_files.isEmpty())
    {
        ui->statusBar->showMessage("没有查找到相关文件", 5000);
        return;
    }

    QString str = ui->lineEditString->text();
    if(str.isEmpty())
    {
        ui->statusBar->showMessage("请输入查找内容", 5000);
        return;
    }
    searchString(str);
}

void MainWindow::on_tableWidget_customContextMenuRequested(const QPoint &pos)
{
    Q_UNUSED(pos)

    m_menu->exec(QCursor::pos());
}

void MainWindow::on_actionOpenDir_triggered()
{
    int row = ui->tableWidget->currentRow();

    QTableWidgetItem *item = ui->tableWidget->item(row, 0);
    if(Q_NULLPTR == item)
    {
        ui->statusBar->showMessage("当前数据为空，无法打开目录", 5000);
        return;
    }

    QString folder = item->text();
    folder = folder.left(folder.lastIndexOf('/'));

    QDesktopServices::openUrl(QUrl::fromLocalFile(folder));
}
